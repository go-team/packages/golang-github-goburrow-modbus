Source: golang-github-goburrow-modbus
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-goburrow-serial-dev
Standards-Version: 4.5.1
Homepage: https://github.com/goburrow/modbus
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-goburrow-modbus
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-goburrow-modbus.git
XS-Go-Import-Path: github.com/goburrow/modbus
Testsuite: autopkgtest-pkg-go

Package: golang-github-goburrow-modbus-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-goburrow-serial-dev
Description: fault-tolerant implementation of modbus protocol
 This package contains a fault-tolerant, fail-fast implementation of
 Modbus protocol in Go.  Supported functions
  - Bit access:
    *   Read Discrete Inputs
    *   Read Coils
    *   Write Single Coil
    *   Write Multiple Coils
  - 16-bit access:
    *   Read Input Registers
    *   Read Holding Registers
    *   Write Single Register
    *   Write Multiple Registers
    *   Read/Write Multiple Registers
    *   Mask Write Register
    *   Read FIFO Queue
